package fr.samir.ahrioui.tp5_projet_android.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import static android.content.Context.MODE_PRIVATE;
import static fr.samir.ahrioui.tp5_projet_android.controller.ConfigurationFragment.API_URL;
import static fr.samir.ahrioui.tp5_projet_android.controller.ConfigurationFragment.PREFS;

public class UpdateBroadcastReceiver extends BroadcastReceiver {
    private SharedPreferences sharedPreferences;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(UpdateBroadcastReceiver.class.getSimpleName(), "onReceive");
        sharedPreferences = context.getSharedPreferences(PREFS, MODE_PRIVATE);

        String strUrl = sharedPreferences.getString(API_URL, "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/diplome/2-L3IN");

        SyncIntentService.startActionFoo(context, strUrl, null);
    }
}
