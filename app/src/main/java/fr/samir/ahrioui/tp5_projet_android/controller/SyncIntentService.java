package fr.samir.ahrioui.tp5_projet_android.controller;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.model.Calendar;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import fr.samir.ahrioui.tp5_projet_android.model.Course;
import fr.samir.ahrioui.tp5_projet_android.model.CourseDbHelper;
import fr.samir.ahrioui.tp5_projet_android.webservice.ICalParser;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class SyncIntentService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_FOO = "fr.samir.ahrioui.tp5_projet_android.controller.action.FOO";
    private static final String ACTION_BAZ = "fr.samir.ahrioui.tp5_projet_android.controller.action.BAZ";

    // TODO: Rename parameters
    private static final String EXTRA_PARAM1 = "fr.samir.ahrioui.tp5_projet_android.controller.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "fr.samir.ahrioui.tp5_projet_android.controller.extra.PARAM2";

    public SyncIntentService() {
        super("SyncIntentService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionFoo(Context context, String param1, String param2) {
        Intent intent = new Intent(context, SyncIntentService.class);
        intent.setAction(ACTION_FOO);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionBaz(Context context, String param1, String param2) {
        Intent intent = new Intent(context, SyncIntentService.class);
        intent.setAction(ACTION_BAZ);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_FOO.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                handleActionFoo(param1, param2);
            } else if (ACTION_BAZ.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                handleActionBaz(param1, param2);
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void handleActionFoo(String param1, String param2) {

        try {
            if (param1 == null || param1.isEmpty())
                param1 = "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/diplome/2-L3IN";
            URL url = new URL(param1);
            URLConnection urlConnection = url.openConnection();
            //Log.d("exception connect :", "Update Shedule");

            // urlConnection.connect();
            InputStream inputStream = urlConnection.getInputStream();
            Log.d("inputStream :", inputStream.toString());
            CalendarBuilder builder = new CalendarBuilder();
            Calendar calendar = builder.build(inputStream);
            ICalParser parser = new ICalParser(calendar);
            List<Course> mCourses = parser.parseCalendar();
            CourseDbHelper courseDbHelper = new CourseDbHelper(getApplicationContext());
            courseDbHelper.populate(mCourses);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent intent = new Intent("fr.samir.ahrioui.tp5_projet_android.controller.STOP_REFRESH");
        // you can also add extras to this intent
        // intent.putExtra("key", value);

        sendBroadcast(intent);

        //throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionBaz(String param1, String param2) {
        // TODO: Handle action Baz
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
