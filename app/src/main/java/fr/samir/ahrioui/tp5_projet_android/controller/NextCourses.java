package fr.samir.ahrioui.tp5_projet_android.controller;

import android.content.Context;
import android.icu.text.SimpleDateFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Parcelable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import fr.samir.ahrioui.tp5_projet_android.R;
import fr.samir.ahrioui.tp5_projet_android.model.Course;
import fr.samir.ahrioui.tp5_projet_android.model.CourseDbHelper;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NextCourses.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NextCourses#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NextCourses extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";


    // TODO: Rename and change types of parameters
    private List<Course> mParam1;
    private CourseDbHelper mCourseDbHelper;
    private OnFragmentInteractionListener mListener;
    private TextView description, description2, startDate, startDate2, endDate, endDate2, timer, timerLabel;
    private Course mCourse1, mCourse2;

    public NextCourses() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment NextCourses.
     */

    public static NextCourses newInstance(List<Course> param1) {
        NextCourses fragment = new NextCourses();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM1, (ArrayList<? extends Parcelable>) param1);


        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        if (getArguments() != null) {
            mParam1 = getArguments().getParcelableArrayList(ARG_PARAM1);
            mCourse1 = mParam1.get(0);
            mCourse2 = mParam1.get(1);


        } else {
            updateData();
            // mCourse1.setStartDate("2019-04-30 20:38:00");
        }


    }

    public boolean updateData() {
        mCourseDbHelper = new CourseDbHelper(getContext());

        mParam1 = mCourseDbHelper.get2FirstCoursesNext();

        if (mParam1 != null && !mParam1.isEmpty()) {
            mCourse1 = mParam1.get(0);

            mCourse2 = mParam1.get(1);

            return true;

        }
        return false;
    }

    public void updateView() {
        startDate.setText(mCourse1.getStartDate());
        endDate.setText(mCourse1.getEndDate());
        description.setText(mCourse1.getDescription());

        startDate2.setText(mCourse2.getStartDate());
        endDate2.setText(mCourse2.getEndDate());
        description2.setText(mCourse2.getDescription());

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public long getRemainingTime() {
        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date nextCourseDate = null;
        try {
            nextCourseDate = dateFormat.parse(mCourse1.getStartDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return nextCourseDate.getTime() - currentDate.getTime();
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View myFragmentView = inflater.inflate(R.layout.fragment_next_courses, container, false);
        description = myFragmentView.findViewById(R.id.next_course_description);
        description2 = myFragmentView.findViewById(R.id.next_course_description2);
        startDate = myFragmentView.findViewById(R.id.next_course_start);
        startDate2 = myFragmentView.findViewById(R.id.next_course_start2);
        endDate = myFragmentView.findViewById(R.id.next_course_end);
        endDate2 = myFragmentView.findViewById(R.id.next_course_end2);
        timer = myFragmentView.findViewById(R.id.next_course_timer);
        timerLabel = myFragmentView.findViewById(R.id.next_timer_label);


        if (mCourse1 != null && mCourse2 != null) {

            updateView();


            new MyCountDownTimer(getRemainingTime(), 1000).start();

        }
        return myFragmentView;
    }

    public class MyCountDownTimer extends CountDownTimer {
        private long millisInFuture;

        MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            this.millisInFuture = millisInFuture;
            timerLabel.setText("Commence dans ");
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void onFinish() {
            millisInFuture = getRemainingTime();
            timerLabel.setText("A commencé depuis ");
            long totalSeconds = (1 * 60) + millisInFuture / 1000;
            CountDownTimer timerUp = new CountDownTimer(totalSeconds * 1000, 1000) {

                public void onTick(long millisUntilFinished) {
                    long timeElapsed = (totalSeconds * 1000 - millisUntilFinished - millisInFuture) / 1000;
                    long min = timeElapsed / 60;
                    timer.setText(min + " min " + (timeElapsed - (min * 60)) + "s");
                }

                @RequiresApi(api = Build.VERSION_CODES.N)
                public void onFinish() {
                    Log.d("done!", "Time's up!");
                    updateData();
                    updateView();
                    new MyCountDownTimer(getRemainingTime(), 1000).start();


                }

            };
            timerUp.start();
        }

        @Override
        public void onTick(long millisUntilFinished) {
            long millis = millisUntilFinished;
            String hms = (TimeUnit.MILLISECONDS.toDays(millis)) + "j "
                    + (TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)) + "h ")
                    + (TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)) + "min "
                    + (TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)) + "s"));
            timer.setText(hms);
        }
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
