package fr.samir.ahrioui.tp5_projet_android.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import fr.samir.ahrioui.tp5_projet_android.R;
import fr.samir.ahrioui.tp5_projet_android.model.Course;


public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

    private List<Course> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context mContext;

    // data is passed into the constructor
    public MyRecyclerViewAdapter(Context context, List<Course> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
    }

    public void setData(List<Course> data) {
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.row, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Course Course = mData.get(position);
        holder.startDate.setText(Course.getStartDate());
        holder.endDate.setText(Course.getEndDate());
        holder.description.setText(Course.getDescription());

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    public int deleteItem(int position) {
        int mRecentlyDeletedItem = (int) mData.get(position).getId();
        mData.remove(position);
        notifyItemRemoved(position);
        return mRecentlyDeletedItem;
    }

    // convenience method for getting data at click position
    public Course getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView startDate, endDate, description;

        ViewHolder(View itemView) {
            super(itemView);
            startDate = itemView.findViewById(R.id.next_course_start);
            endDate = itemView.findViewById((R.id.next_course_end2));
            description = itemView.findViewById(R.id.day_course_description);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }
}
