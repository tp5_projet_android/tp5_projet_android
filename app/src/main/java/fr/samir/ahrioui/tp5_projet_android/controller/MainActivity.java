package fr.samir.ahrioui.tp5_projet_android.controller;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import net.fortuna.ical4j.util.MapTimeZoneCache;

import fr.samir.ahrioui.tp5_projet_android.R;
import fr.samir.ahrioui.tp5_projet_android.model.CourseDbHelper;

import static fr.samir.ahrioui.tp5_projet_android.controller.ConfigurationFragment.API_URL;
import static fr.samir.ahrioui.tp5_projet_android.controller.ConfigurationFragment.PREFS;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        NextCourses.OnFragmentInteractionListener, SelectDay.OnFragmentInteractionListener,
        DayCourses.OnFragmentInteractionListener, NextEvals.OnFragmentInteractionListener,
        ConfigurationFragment.OnFragmentInteractionListener, SwipeRefreshLayout.OnRefreshListener {

    private CourseDbHelper mCourseDbHelper;
    private SharedPreferences sharedPreferences;
    private SwipeRefreshLayout mRefreshLayout;
    private BroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.setProperty("net.fortuna.ical4j.timezone.cache.impl", MapTimeZoneCache.class.getName());

        // List<Course> data = mCourseDbHelper.get2FirstCoursesNext();

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sharedPreferences = this.getSharedPreferences(PREFS, MODE_PRIVATE);
        mCourseDbHelper = new CourseDbHelper(this);
        mRefreshLayout = findViewById(R.id.swipeRefresh);
        mRefreshLayout.setOnRefreshListener(this);
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // stop refreshing the layout
                mRefreshLayout.setRefreshing(false);
                getSupportFragmentManager().getFragments();
                for (Fragment f : getSupportFragmentManager().getFragments()) {
                    if (f.isResumed()) {
                        try {

                            loadFragment(f.getClass().newInstance());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
        };
        registerReceiver(receiver, new IntentFilter("fr.samir.ahrioui.tp5_projet_android.controller.STOP_REFRESH"));

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState == null) {
            loadFragment(new NextCourses());
            navigationView.setCheckedItem(R.id.nav_next_courses);
        }

        Intent updateIntent = new Intent(getApplicationContext(), UpdateBroadcastReceiver.class);
        PendingIntent updateAlarmIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, updateIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        long startTime = System.currentTimeMillis(); //alarm starts immediately
        AlarmManager backupAlarmMgr = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        backupAlarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, startTime, 60 * 1000, updateAlarmIntent); // alarm will repeat after every 15 minutes



    }

    @Override
    protected void onDestroy() {
        // unregister the receiver
        unregisterReceiver(receiver);
        super.onDestroy();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_sync) {
            String strUrl = sharedPreferences.getString(API_URL, "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/diplome/2-L3IN");
            mRefreshLayout.setRefreshing(true);
            SyncIntentService.startActionFoo(this, strUrl, null);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_next_courses) {
            loadFragment(new NextCourses());

        } else if (id == R.id.nav_today_courses) {

            loadFragment(new DayCourses());
        } else if (id == R.id.nav_select_day) {
            loadFragment(new SelectDay());

        } else if (id == R.id.nav_next_evaluations) {

            loadFragment(new NextEvals());
        } else if (id == R.id.nav_config) {

            loadFragment(new ConfigurationFragment());
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void loadFragment(Fragment fragment) {
// create a FragmentManager
        FragmentManager fm = getSupportFragmentManager();
// create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
// replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.commit(); // save the changes
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onRefresh() {
        String strUrl = sharedPreferences.getString(API_URL, "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/diplome/2-L3IN");
        mRefreshLayout.setRefreshing(true);
        SyncIntentService.startActionFoo(this, strUrl, null);
    }

}
